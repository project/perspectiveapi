## Perspective API

_Perspective API_ integrates <a href="https://www.perspectiveapi.com/">Google perspective</a>.

### About Perspective API
The Perspective API is part of the <a href="https://conversationai.github.io/">ConversationAI</a>
research effort that aimsaims to help increase participation, quality, and
empathy in online conversation at scale.
